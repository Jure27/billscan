package jure.billscan;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends Activity implements SurfaceHolder.Callback {

    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;
    private LayoutInflater mInflater = null;
    Camera mCamera;
    byte[] tempData;
    boolean mPreviewRunning = false;
    Button takePicture;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        */
        mSurfaceView = (SurfaceView)findViewById(R.id.surface);
        mSurfaceHolder = mSurfaceView.getHolder();
        mSurfaceHolder.addCallback(this);
        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mInflater = LayoutInflater.from(this);
        View overView = mInflater.inflate(R.layout.cameraoverlay,null);
        this.addContentView(overView, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        takePicture = (Button) findViewById(R.id.buttonCamera);
        takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Camera.ShutterCallback mShutterCallBack = new Camera.ShutterCallback() {
                    @Override
                    public void onShutter() {

                    }
                };

                Camera.PictureCallback mPictureCallback = new Camera.PictureCallback() {
                    @Override

                    public void onPictureTaken(byte[] data, Camera c) {}
                };
                        Camera.PictureCallback mjpeg = new Camera.PictureCallback() {
                            @Override
                            public void onPictureTaken(byte[] data, Camera c) {
                                if(data != null){
                                    tempData = data;
                                    done();
                                }
                            }
                        ;


                    void done(){
                        Bitmap bm = BitmapFactory.decodeByteArray(tempData, 0, tempData.length);
                        String url = MediaStore.Images.Media.insertImage(getContentResolver(), bm ,null, null);
                        bm.recycle();
                        Bundle bundle = new Bundle();
                        if(url != null){
                            bundle.putString("url", url);
                            Intent mIntent = new Intent();
                            mIntent.putExtras(bundle);
                            setResult(RESULT_OK, mIntent);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Can't save picture",Toast.LENGTH_LONG).show();
                        }
                        finish();
                    }

                        };
                mCamera.takePicture(mShutterCallBack,mPictureCallback, mjpeg);
            }
        });




        };












    @Override
    protected void onResume() {
        super.onResume();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_folder:
                Intent intent = new Intent(MainActivity.this,folderList.class);
                startActivity(intent);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.d("surface","Surface created");
        mCamera.open();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        Log.e("surface", "surface changed");
        try{
            if(mPreviewRunning){
                mCamera.stopPreview();
                mPreviewRunning = false;
            }
            Camera.Parameters p = mCamera.getParameters();
            p.setPreviewSize(w, h);
            mCamera.setParameters(p);
            try {
                mCamera.setPreviewDisplay(holder);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mCamera.startPreview();
            mPreviewRunning = true;
        } catch (Exception e){
            Log.d("", e.toString());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        Log.e("surface","Surface destroyed");
        mCamera.stopPreview();
        mPreviewRunning = false;
        mCamera.release();
        mCamera = null;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}
