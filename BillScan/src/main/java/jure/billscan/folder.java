package jure.billscan;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class folder extends Activity {

    //final ArrayList<String> names = new ArrayList<String>();
    String[] nameValues = new String[] {"Banane", "Petrol", "Rezervni deli" };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.folder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_camera:
                Intent intent = new Intent(folder.this,MainActivity.class);
                startActivity(intent);
            case android.R.id.home:
               this.finish();



        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_folder, container, false);

            LinearLayout scrollContent = (LinearLayout) rootView.findViewById(R.id.scrollContent);
            for(int i = 0; i < 3; i++){


                ViewGroup receipte=  (ViewGroup) inflater.inflate(R.layout.receipte, null);
                ViewGroup blankView=  (ViewGroup) inflater.inflate(R.layout.blank_view, null);

                ImageView viewImage = (ImageView) receipte.findViewById(R.id.imageView);
                viewImage.setImageResource(R.drawable.receipt);
                TextView viewName = (TextView) receipte.findViewById(R.id.name);
                viewName.setText(nameValues[i]);
                TextView viewWarranty = (TextView) receipte.findViewById(R.id.warranty);
                viewWarranty.setText("Do: 24.3.2014");
                TextView viewDate = (TextView) receipte.findViewById(R.id.date);
                viewDate.setText("Izdan: 24.1.2014");
                TextView viewUser = (TextView) receipte.findViewById(R.id.user);
                viewUser.setText("Uporabnik: Jure");

                scrollContent.addView(receipte);
                scrollContent.addView(blankView);



            }
            return rootView;
        }
    }

}
