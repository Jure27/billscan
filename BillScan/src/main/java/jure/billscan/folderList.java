package jure.billscan;


import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class folderList extends Activity {
    final ArrayList<String> list = new ArrayList<String>();
    StableArrayAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        final ListView listview = (ListView) findViewById(R.id.listview);
        String[] values = new String[] {"Luka", "Toyota", "Futuring" };


        for (int i = 0; i < values.length; ++i) {
            list.add(values[i]);
        }
        adapter = new StableArrayAdapter(this,
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, final View view,int position, long id) {
                final String item = (String) parent.getItemAtPosition(position);

                Intent intent = new Intent(folderList.this,folder.class);
                startActivity(intent);

               /* view.animate().setDuration(2000).alpha(0).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                list.remove(item);
                                adapter.notifyDataSetChanged();
                                view.setAlpha(1);
                            }
                        });*/
            }

        });
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.folder_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
            case R.id.action_search:
                return true;
            case R.id.action_camera:
                Intent intent = new Intent(folderList.this,MainActivity.class);
                startActivity(intent);
            case R.id.action_addFolder:
                Log.d("PopUP","addFolder selected");
                LayoutInflater layoutInflater = (LayoutInflater)getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.add_file, null);
                final PopupWindow popupWindow = new PopupWindow(popupView,ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                Button buttonAdd = (Button)popupView.findViewById(R.id.buttonAdd);
                Button buttonCancel = (Button)popupView.findViewById(R.id.buttonCancel);
                final ListView listview = (ListView) findViewById(R.id.listview);
                popupWindow.showAtLocation(listview, Gravity.CENTER,0,200);
                Log.d("PopUP","Pop up window should show");
                buttonCancel.setOnClickListener(new Button.OnClickListener(){

                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                    }
                });


                buttonAdd.setOnClickListener(new Button.OnClickListener(){
                    EditText editTextFolder = (EditText)popupView.findViewById(R.id.editText);
                    @Override
                    public void onClick(View view) {
                        if(editTextFolder.getText()!= null){
                            list.add(editTextFolder.getText().toString());
                            adapter = new StableArrayAdapter(folderList.this,
                                    android.R.layout.simple_list_item_1, list);
                            listview.setAdapter(adapter);
                            popupWindow.dismiss();
                        }
                    }
                });

        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_folder_list, container, false);
            return rootView;
        }
    }

}
